#!/bin/bash

# Verificar se foram fornecidos dois parâmetros
if [ $# -ne 2 ]; then
    echo "Uso: $0 <numero1> <numero2>"
    exit 1
fi

# Atribuir os parâmetros a variáveis
a=$1
b=$2

# Solicitar ao usuário que digite a operação
read -p "Digite a operação (+, -, , /, *): " operacao

# Realizar a operação e imprimir o resultado
case $operacao in
    "+") resultado=$((a + b)) ;;
    "-") resultado=$((a - b)) ;;
    "*") resultado=$((a * b)) ;;
    "/") resultado=$((a / b)) ;;
    "*") resultado=$((a * b)) ;;
    *) echo "Operação inválida. Saindo."; exit 1 ;;
esac

echo "Resultado de $a $operacao $b = $resultado"
