#!/bin/bash

# Verificar se foram fornecidos dois parâmetros
if [ $# -ne 2 ]; then
    echo "Uso: $0 <numero1> <numero2>"
    exit 1
fi

# Atribuir os parâmetros a variáveis
numero1=$1
numero2=$2

# Utilizar o programa externo 'bc' para realizar a soma
resultado=$(echo "$numero1 + $numero2" | bc)

echo "Resultado de $numero1 + $numero2 = $resultado"
