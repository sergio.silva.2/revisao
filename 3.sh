#!/bin/bash

# Verificar se foram fornecidos dois parâmetros
if [ $# -ne 2 ]; then
    echo "Uso: $0 <diretorio1> <diretorio2>"
    exit 1
fi

# Atribuir os parâmetros a variáveis
diretorio1=$1
diretorio2=$2

# Listar o conteúdo dos diretórios e salvar em 03.txt
{
    echo "Conteúdo do diretório $diretorio1:"
    ls -l "$diretorio1"
    echo "-----"
    echo "Conteúdo do diretório $diretorio2:"
    ls -l "$diretorio2"
} > 03.txt

echo "Listas de diretórios foram salvas em 03.txt"
