#!/bin/bash

echo "Lista de 20 variáveis automáticas no Bash:"

# 1. $0 - Nome do script
echo "1. \$0 - Nome do script: $0"

# 2. $1, $2, ..., $n - Argumentos passados para o script
echo "2. \$1 - Primeiro argumento passado para o script: $1"
echo "3. \$2 - Segundo argumento passado para o script: $2"

# 4. $# - Número total de argumentos passados para o script
echo "4. \$# - Número total de argumentos passados para o script: $#"

# 5. $* - Todos os argumentos passados para o script como uma única string
echo "5. \$* - Todos os argumentos passados para o script como uma única string: $*"

# 6. $@ - Todos os argumentos passados para o script como uma lista de strings
echo "6. \$@ - Todos os argumentos passados para o script como uma lista de strings: $@"

# 7. $? - Código de retorno do último comando executado
echo "7. \$? - Código de retorno do último comando executado: $?"

# 8. $$ - PID (identificador de processo) do script em execução
echo "8. \$$ - PID (identificador de processo) do script em execução: $$"

# 9. $! - PID do último comando em segundo plano
echo "9. \$! - PID do último comando em segundo plano: $!"

# 10. $RANDOM - Número inteiro aleatório entre 0 e 32767
echo "10. \$RANDOM - Número inteiro aleatório entre 0 e 32767: $RANDOM"

# 11. $LINENO - Número da linha atual no script
echo "11. \$LINENO - Número da linha atual no script: $LINENO"

# 12. $PS1 - Prompt do shell primário
echo "12. \$PS1 - Prompt do shell primário: $PS1"

# 13. $PS2 - Prompt do shell secundário
echo "13. \$PS2 - Prompt do shell secundário: $PS2"

# 14. $IFS - Separador de campos interno (Internal Field Separator)
echo "14. \$IFS - Separador de campos interno (Internal Field Separator): $IFS"

# 15. $HOME - Diretório home do usuário
echo "15. \$HOME - Diretório home do usuário: $HOME"

# 16. $PWD - Diretório atual
echo "16. \$PWD - Diretório atual: $PWD"

# 17. $OLDPWD - Diretório anterior
echo "17. \$OLDPWD - Diretório anterior: $OLDPWD"

# 18. $USER - Nome do usuário
echo "18. \$USER - Nome do usuário: $USER"

# 19. $HOSTNAME - Nome do host
echo "19. \$HOSTNAME - Nome do host: $HOSTNAME"

# 20. $SECONDS - Número de segundos desde que o script foi iniciado
echo "20. \$SECONDS - Número de segundos desde que o script foi iniciado: $SECONDS"

# Adicione outras variáveis automáticas conforme necessário

exit 0
